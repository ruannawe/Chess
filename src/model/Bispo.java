package model;

import java.awt.Point;
import java.util.ArrayList;



public class Bispo extends Piece{

	public Bispo(String imagePath) {
		super(imagePath);
	}

	public Bispo(String imagePath, String typeOfObject) {
		super(imagePath, typeOfObject);
	}
	
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		
		ArrayList<Point> movesBispo = new ArrayList<>();
		
		for(int i = x, j = y;( i <7 || j <7) ; i++, j++) {					

			/* BISPO NAS BORDAS DIREITA E INFERIOR*/
			if(i == 7 || j == 7) {
				break ;
			}
			
			Point pointBD = new Point(i+1, j+1);
			movesBispo.add(pointBD);
			
		}

		for(int i = x, j = y; i< 7 || j>0; i++, j--) {

			/* BISPO NAS BORDAS ESQUERDA E INFERIOR*/
			if(i == 7 || j == 0) {
				break ;
			}
			
			Point pointBE = new Point(i+1, j-1);
			movesBispo.add(pointBE);
			
		}

		for(int i = x, j = y; i>0 || j<7; i--, j++) {

			/* BISPO NAS BORDAS DIREITA E SUPERIOR*/
			if(i == 0 || j == 7) {
				break;
			}

			Point pointCD = new Point(i-1, j+1);
			movesBispo.add(pointCD);
		
		}
		for(int i = x, j = y; i>0 || j>0; i--, j--) {

			/* BISPO NAS BORDAS DIREITA E SUPERIOR*/
			if(i == 0 || j == 0) {
				break;
			}

			Point pointCD = new Point(i-1, j-1);
			movesBispo.add(pointCD);
			
		}
	
		return movesBispo;
	}
}
