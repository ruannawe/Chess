package model;

import java.awt.Point;
import java.util.ArrayList;

import javax.swing.text.Position;

public abstract class Piece {
	private String imagePath;
	private String typeOfObject;
	private ArrayList<Position> moves;
	
	public Piece(String imagePath) {
		super();
		this.imagePath = imagePath;
	}

	public Piece(String imagePath, String typeOfObject) {
		super();
		this.imagePath = imagePath;
		this.typeOfObject = typeOfObject;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getTypeOfObject() {
		return typeOfObject;
	}

	public void setTypeOfObject(String typeOfObject) {
		this.typeOfObject = typeOfObject;
	}

	public ArrayList<Position> getMoves() {
		return moves;
	}
	public void setMoves(ArrayList<Position> moves) {
		this.moves = moves;
	}

	public ArrayList<Point> getMoves(int x, int y) {
				return null;
	}
	public ArrayList<Point> getMovesW(int x, int y) {
		return null;
	}
	public ArrayList<Point> getMovesB(int x, int y) {
		return null;
}
	
}
