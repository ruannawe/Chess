package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rainha extends Piece{

	public Rainha(String imagePath, String typeOfObject) {
		super(imagePath, typeOfObject);
	}

	public Rainha(String imagePath) {
		super(imagePath);
	}
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		ArrayList<Point> movesRainha = new ArrayList<>();
		
		for(int i = y+1; i<8; i++) {
			Point pointD = new Point(x, i);
			movesRainha.add(pointD);
		}
		for(int i = y-1; i>=0; i--) {
			Point pointE = new Point(x, i);
			movesRainha.add(pointE);
		}
		
		for(int i = x+1; i<8; i++) {
			Point pointC = new Point(i, y);
			movesRainha.add(pointC);
		}
		for(int i = x-1; i>=0; i--) {
			Point poinB = new Point(i, y);
			movesRainha.add(poinB);
		}
		for(int i = x, j = y;( i <7 || j <7) ; i++, j++) {					

			/* BISPO NAS BORDAS DIREITA E INFERIOR*/
			if(i == 7 || j == 7) {
				break ;
			}
			
			Point pointBD = new Point(i+1, j+1);
			movesRainha.add(pointBD);
			
		}

		for(int i = x, j = y; i< 7 || j>0; i++, j--) {

			/* BISPO NAS BORDAS ESQUERDA E INFERIOR*/
			if(i == 7 || j == 0) {
				break ;
			}
			
			Point pointBE = new Point(i+1, j-1);
			movesRainha.add(pointBE);
			
		}

		for(int i = x, j = y; i>0 || j<7; i--, j++) {

			/* BISPO NAS BORDAS DIREITA E SUPERIOR*/
			if(i == 0 || j == 7) {
				break;
			}

			Point pointCD = new Point(i-1, j+1);
			movesRainha.add(pointCD);
		
		}
		for(int i = x, j = y; i>0 || j>0; i--, j--) {

			/* BISPO NAS BORDAS DIREITA E SUPERIOR*/
			if(i == 0 || j == 0) {
				break;
			}

			Point pointCD = new Point(i-1, j-1);
			movesRainha.add(pointCD);
			
		}
		
		return movesRainha;
	}
}
