package control;

import java.awt.Color;
import java.util.ArrayList;
import Teste.TestesUnitarios;
import model.Piece;
import model.Square;
import model.Square.SquareEventListener;
import model.outOfPossibleSquaresException;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_MOVE = new Color(0x66B0FF);
	public static final Color DEFAULT_COLOR_RED = Color.RED;
	
	
	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Color colorMove;
	private Color colorRed;
	
	private Square selectedSquare;
	TestesUnitarios t = new TestesUnitarios();
	private ArrayList<Square> squareList;
	private ArrayList<Square> possibleSquares;
	
	
	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED, DEFAULT_COLOR_MOVE, DEFAULT_COLOR_RED);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color colorMove, Color colorRed) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.colorMove = colorMove;
		this.colorRed = colorRed;
		
		this.squareList = new ArrayList<>();
		this.possibleSquares = new ArrayList<>();
		
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		System.out.println("X = " + square.getPosition().x + "Y = " + square.getPosition().y);
		square.setColor(this.colorHover);
	}

	@Override
	public void onSelectEvent(Square square) throws outOfPossibleSquaresException {
		
		if (haveSelectedCellPanel()) {
			if(!possibleSquares.contains(square) && !this.selectedSquare.equals(square))
			{
				throw new outOfPossibleSquaresException("Você não pode acessar o quadrado selecionado, favor, clicar em um quadrado azul ou vermelho");
			}
			if (!this.selectedSquare.equals(square) && possibleSquares.contains(square)) { 
				moveContentOfSelectedSquare(square);
				possibleSquares.clear();
			} 
			else {
				unselectSquare(square);
			}
		} 
		else {
		
			selectSquare(square);
		}
	}

	@Override
	public void onOutEvent(Square square) {
		
		if (this.selectedSquare != square && !possibleSquares.contains(square) ) {	
			resetColor(square);
		} 
		else {
			if(this.selectedSquare == square)
				square.setColor(this.colorSelected);		
			else
				square.setColor(new Color(0x66B0FF));
		}
		
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		square.setPiece(this.selectedSquare.getPiece());
		this.selectedSquare.removePiece();
		unselectSquare(square);
	}

	private void selectSquare(Square square) {
		if (square.havePiece()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			this.showPossibleMoves(this.selectedSquare);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		
		for(int i = 0; i<possibleSquares.size(); i++)
			resetColor(possibleSquares.get(i));
		possibleSquares.clear();
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
	
	private void possiveisMovimentosDoPeaoWhite(Square square) {
		
		if(square.getPosition().x==0) {
			System.out.println("Não é possível movimentar o peão");
		}
		else {
			t.setRetornoEsperadoDeY(square.getPosition().y);
			t.setRetornoEsperadoDeX(square.getPosition().x);
			if(square.getPosition().x == 6) {
				Square square1 = getSquare(square.getPosition().x - 1, square.getPosition().y);
				Square square2 = getSquare(square.getPosition().x - 2 , square.getPosition().y);
				possibleSquares.add(square1);
				possibleSquares.add(square2);
			
				square1.setColor(this.colorMove);
				square2.setColor(this.colorMove);
			}
			else {
				Square squareEsquerdo = getSquare(square.getPosition().x-1, square.getPosition().y-1);
				Square squareDireito = getSquare(square.getPosition().x-1, square.getPosition().y+1);
				Square squareFrente = getSquare(square.getPosition().x-1, square.getPosition().y);			
				if(!squareFrente.havePiece()) {
					squareFrente.setColor(this.colorMove);
					possibleSquares.add(squareFrente);
				}
				if(square.getPosition().y != 0 && squareEsquerdo.havePiece()) {
					if(squareEsquerdo.getPiece().getImagePath().contains("Brown")) {
						squareEsquerdo.setColor(this.colorRed);
						possibleSquares.add(squareEsquerdo);
					}
				}
				if (square.getPosition().y != 7 && squareDireito.havePiece()) {
					if (squareDireito.getPiece().getImagePath().contains("Brown")) {
						squareDireito.setColor(this.colorRed);
						possibleSquares.add(squareDireito);
					}
				}			
			}
			t.testePeaoWhite();
			t.setPosicaoX(square.getPosition().x);
			t.setPosicaoY(square.getPosition().y);
		}
	}
	
	private void possiveisMovimentosDoPeaoBrown(Square square) {
		
		if(square.getPosition().x==7) {
			System.out.println("não é possivel movimentar o peão");
		}
		else {
			t.setRetornoEsperadoDeY(square.getPosition().y);
			t.setRetornoEsperadoDeX(square.getPosition().x);
			if(square.getPosition().x == 1) {
				Square square1 = getSquare(square.getPosition().x + 1, square.getPosition().y);
				Square square2 = getSquare(square.getPosition().x + 2 , square.getPosition().y);
				possibleSquares.add(square1);
				possibleSquares.add(square2);
				square1.setColor(this.colorMove);
				square2.setColor(this.colorMove);
			}
			else {
				Square squareEsquerdo = getSquare(square.getPosition().x+1, square.getPosition().y+1);
				Square squareDireito = getSquare(square.getPosition().x+1, square.getPosition().y-1);
				Square squareFrente = getSquare(square.getPosition().x+1, square.getPosition().y);
				
				if(!squareFrente.havePiece()) {
					squareFrente.setColor(this.colorMove);
					possibleSquares.add(squareFrente);
				}
				if(square.getPosition().y != 7 &&squareEsquerdo.havePiece()) {
					if(squareEsquerdo.getPiece().getImagePath().contains("White")) {
						squareEsquerdo.setColor(this.colorRed);
						possibleSquares.add(squareEsquerdo);
					}
				}
				if(square.getPosition().y != 0 && squareDireito.havePiece()) {
					if(squareDireito.getPiece().getImagePath().contains("White")) {
						squareDireito.setColor(this.colorRed);
						possibleSquares.add(squareDireito);
					}
				}
			}
			t.testePeaoBrown();
			t.setPosicaoX(square.getPosition().x);
			t.setPosicaoY(square.getPosition().y);
		}	
	}
	
	private void possiveisMovimentosDaTorreWhite(Square square) {
		t.setRetornoEsperadoDeY(square.getPosition().y);
		t.setRetornoEsperadoDeX(square.getPosition().x);
		for(int i = square.getPosition().y+1; i<8; i++) {
			Square squareD = getSquare(square.getPosition().x, i);
			if(squareD.havePiece()) 
			{
				if(squareD.getPiece().getImagePath().contains("Brown")) {
					squareD.setColor(this.colorRed);
					possibleSquares.add(squareD);
					break;
				}
				else {
					break;

				}
			}
			squareD.setColor(this.colorMove);
			possibleSquares.add(squareD);
		}
		
		for(int i = square.getPosition().y-1; i>=0; i--) {
			Square squareE = getSquare(square.getPosition().x, i);
			if(squareE.havePiece()) 
			{
				if(squareE.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareE.setColor(this.colorRed);
					possibleSquares.add(squareE);
					break;
				}
			}
			squareE.setColor(this.colorMove);
			possibleSquares.add(squareE);
		}
		
		for(int i = square.getPosition().x+1; i<8; i++) {
			Square squareC = getSquare(i, square.getPosition().y);
			if(squareC.havePiece()) 
			{
				if(squareC.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareC.setColor(this.colorRed);
					possibleSquares.add(squareC);
					break;
				}
			}
			squareC.setColor(this.colorMove);
			possibleSquares.add(squareC);
		}
		
		for(int i = square.getPosition().x-1; i>=0; i--) {
			Square squareB = getSquare(i, square.getPosition().y);
			if(squareB.havePiece()) 
			{
				if(squareB.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareB.setColor(this.colorRed);
					possibleSquares.add(squareB);
					break;
				}
			}
			squareB.setColor(this.colorMove);
			possibleSquares.add(squareB);
		}
		t.testeTorreWhite();
		t.setPosicaoX(square.getPosition().x);
		t.setPosicaoY(square.getPosition().y);
	}

	private void possiveisMovimentosDaTorreBrown(Square square) {
		t.setRetornoEsperadoDeY(square.getPosition().y);
		t.setRetornoEsperadoDeX(square.getPosition().x);
		for(int i = square.getPosition().y+1; i<8; i++) {
			Square squareD = getSquare(square.getPosition().x, i);
			if(squareD.havePiece()) 
			{
				if(squareD.getPiece().getImagePath().contains("White")) {
					squareD.setColor(this.colorRed);
					possibleSquares.add(squareD);
					break;
				}
				else {
					break;

				}
			}
			squareD.setColor(this.colorMove);
			possibleSquares.add(squareD);
		}
		for(int i = square.getPosition().y-1; i>=0; i--) {
			Square squareE = getSquare(square.getPosition().x, i);
			if(squareE.havePiece()) 
			{
				if(squareE.getPiece().getImagePath().contains("Brown")) 
					break;
				else {
					squareE.setColor(this.colorRed);
					possibleSquares.add(squareE);
					break;
				}
			}
			squareE.setColor(this.colorMove);
			possibleSquares.add(squareE);
		}
		
		for(int i = square.getPosition().x+1; i<8; i++) {
			Square squareC = getSquare(i, square.getPosition().y);
			if(squareC.havePiece()) 
			{
				if(squareC.getPiece().getImagePath().contains("Brown")) 
					break;
				else {
					squareC.setColor(this.colorRed);
					possibleSquares.add(squareC);
					break;
				}
			}
			squareC.setColor(this.colorMove);
			possibleSquares.add(squareC);
		}
		for(int i = square.getPosition().x-1; i>=0; i--) {
			Square squareB = getSquare(i, square.getPosition().y);
			if(squareB.havePiece()) 
			{
				if(squareB.getPiece().getImagePath().contains("Brown")) 
					break;
				else {
					squareB.setColor(this.colorRed);
					possibleSquares.add(squareB);
					break;
				}
			}
			squareB.setColor(this.colorMove);
			possibleSquares.add(squareB);
		}
		t.testeTorreBrown();
		t.setPosicaoX(square.getPosition().x);
		t.setPosicaoY(square.getPosition().y);
	}
	
	private void possiveisMovimentosDoBispoWhite(Square square) {			
		t.setRetornoEsperadoDeY(square.getPosition().y);
		t.setRetornoEsperadoDeX(square.getPosition().x);
		for(int i = square.getPosition().x, j = square.getPosition().y;( i <7 || j <7) ; i++, j++) {					
			
			if(i == 7 || j == 7) {
				break;
			}
		
			Square squareBD = getSquare(i+1, j+1);
			
			if(squareBD.getPosition().y == 7 && !squareBD.havePiece()) {
				squareBD.setColor(this.colorMove);
				possibleSquares.add(squareBD);
				
				break;
			}
			if(squareBD.havePiece()) 
			{
				if(squareBD.getPiece().getImagePath().contains("Brown")) {
					squareBD.setColor(this.colorRed);
					possibleSquares.add(squareBD);
					break;
				}
				else {
					break;

				}
			}
			squareBD.setColor(this.colorMove);
			possibleSquares.add(squareBD);
		}
		
		for(int i = square.getPosition().x, j = square.getPosition().y; i< 7 || j>0; i++, j--) {
			
			if(i == 7 || j == 0) {
				break;
			}
			
			Square squareBE = getSquare(i+1, j-1);
			
			if(squareBE.getPosition().y == 0 && !squareBE.havePiece()) {
				squareBE.setColor(this.colorMove);
				possibleSquares.add(squareBE);
				break;
			}
			
			if(squareBE.havePiece()) 
			{
				if(squareBE.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareBE.setColor(this.colorRed);
					possibleSquares.add(squareBE);
					break;
				}
			}
			
			squareBE.setColor(this.colorMove);
			possibleSquares.add(squareBE);
		}
		
		for(int i = square.getPosition().x, j = square.getPosition().y; i>0 || j<7; i--, j++) {
			
			if(i == 0 || j == 7) {
				
				break ;
			}
			
			Square squareCD = getSquare(i-1, j+1);
			
			if(squareCD.getPosition().y == 7 && !squareCD.havePiece()) {
				squareCD.setColor(this.colorMove);
				possibleSquares.add(squareCD);
				break;
			}
			
			if(squareCD.havePiece()) 
			{
				if(squareCD.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareCD.setColor(this.colorRed);
					possibleSquares.add(squareCD);
					break;
				}
			}
			
			squareCD.setColor(this.colorMove);
			possibleSquares.add(squareCD);
		}
		for(int i = square.getPosition().x, j = square.getPosition().y; i>0 || j>0; i--, j--) {
			
			if(i == 0 || j == 0) {
				break;
			}
			
			
			Square squareCD = getSquare(i-1, j-1);
			
			if(squareCD.getPosition().y == 0 && !squareCD.havePiece()) {
				
				squareCD.setColor(this.colorMove);
				possibleSquares.add(squareCD);
				break;
			}
			
			if(squareCD.havePiece()) 
			{
				if(squareCD.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareCD.setColor(this.colorRed);
					possibleSquares.add(squareCD);
					break;
				}
			}
			squareCD.setColor(this.colorMove);
			possibleSquares.add(squareCD);
		}
		t.testeBispoWhite();
		t.setPosicaoX(square.getPosition().x);
		t.setPosicaoY(square.getPosition().y);
	}	
	private void possiveisMovimentosDoBispoBrown(Square square) {
		t.setRetornoEsperadoDeY(square.getPosition().y);
		t.setRetornoEsperadoDeX(square.getPosition().x);
		for(int i = square.getPosition().x, j = square.getPosition().y;( i <7 || j <7) ; i++, j++) {					

			if(i == 7 || i == 7) {
				break ;
			}
			
			Square squareBD = getSquare(i+1, j+1);

			if(squareBD.getPosition().y == 7 && !squareBD.havePiece()) {
				squareBD.setColor(this.colorMove);
				possibleSquares.add(squareBD);
				break;
			}
			if(squareBD.havePiece()) 
			{
				if(squareBD.getPiece().getImagePath().contains("White")) {
					squareBD.setColor(this.colorRed);
					possibleSquares.add(squareBD);
					break;
				}
				else {
					break;

				}
			}
			squareBD.setColor(this.colorMove);
			possibleSquares.add(squareBD);
		}

		for(int i = square.getPosition().x, j = square.getPosition().y; i< 7 || j>0; i++, j--) {

			if(i == 7 || j == 0) {
				break ;
			}

			Square squareBE = getSquare(i+1, j-1);

			if(squareBE.getPosition().y == 0 && !squareBE.havePiece()) {
				squareBE.setColor(this.colorMove);
				possibleSquares.add(squareBE);
				break;
			}

			if(squareBE.havePiece()) 
			{
				if(squareBE.getPiece().getImagePath().contains("Brown")) 
					break;
				else {
					squareBE.setColor(this.colorRed);
					possibleSquares.add(squareBE);
					break;
				}
			}

			squareBE.setColor(this.colorMove);
			possibleSquares.add(squareBE);
		}

		for(int i = square.getPosition().x, j = square.getPosition().y; i>0 || j<7; i--, j++) {

			if(i == 0 || j == 7) {
				break;
			}

			Square squareCD = getSquare(i-1, j+1);

			if(squareCD.getPosition().y == 7 && !squareCD.havePiece()) {
				squareCD.setColor(this.colorRed);
				possibleSquares.add(squareCD);
				break;
			}

			if(squareCD.havePiece()) 
			{
				if(squareCD.getPiece().getImagePath().contains("Brown")) 
					break;
				else {
					squareCD.setColor(this.colorRed);
					possibleSquares.add(squareCD);
					break;
				}
			}

			squareCD.setColor(this.colorMove);
			possibleSquares.add(squareCD);
		}
		for(int i = square.getPosition().x, j = square.getPosition().y; i>0 || j>0; i--, j--) {

			if(i == 0 || j == 0) {
				break;
			}

			Square squareCD = getSquare(i-1, j-1);

			if(squareCD.getPosition().y == 0 && !squareCD.havePiece()) {
				squareCD.setColor(this.colorMove);
				possibleSquares.add(squareCD);
				break;
			}

			if(squareCD.havePiece()) 
			{
				if(squareCD.getPiece().getImagePath().contains("Brown")) 
					break;
				else {
					squareCD.setColor(this.colorRed);
					possibleSquares.add(squareCD);
					break;
				}
			}

			squareCD.setColor(this.colorMove);
			possibleSquares.add(squareCD);
		}
		t.testeBispoBrown();
		t.setPosicaoX(square.getPosition().x);
		t.setPosicaoY(square.getPosition().y);
	}
	
	private void possiveisMovimentosDaRainhaWhite(Square square) {
		this.possiveisMovimentosDoBispoWhite(square);
		this.possiveisMovimentosDaTorreWhite(square);
	}
	private void possiveisMovimentosDaRainhaBrown(Square square) {
		this.possiveisMovimentosDoBispoBrown(square);
		this.possiveisMovimentosDaTorreBrown(square);
	}
	
	private void possiveisMovimentosDoCavaloWhite(Square square) {
		t.setRetornoEsperadoDeY(square.getPosition().y);
		t.setRetornoEsperadoDeX(square.getPosition().x);
		Square squareDC, squareDB;

		if( !(square.getPosition().y == 6 || square.getPosition().y ==7) ) {

			if(!(square.getPosition().x == 0)) {
				squareDC = getSquare(square.getPosition().x-1, square.getPosition().y+2);
				if(squareDC.havePiece()) {
					if(squareDC.getPiece().getImagePath().contains("Brown")) {
						squareDC.setColor(this.colorRed);
						possibleSquares.add(squareDC);
					}
				}
				else {
					squareDC.setColor(this.colorMove);
					possibleSquares.add(squareDC);
				}

			}

			if(!(square.getPosition().x == 7) ) {
				squareDB = getSquare(square.getPosition().x+1, square.getPosition().y+2);
				if(squareDB.havePiece()) {
					if(squareDB.getPiece().getImagePath().contains("Brown")) {
						squareDB.setColor(this.colorRed);
						possibleSquares.add(squareDB);
					}
				}
				else {
					squareDB.setColor(this.colorMove);
					possibleSquares.add(squareDB);
				}
			}			
		}


		Square squareEC;
		Square squareEB;	
		if( !(square.getPosition().y == 0 || square.getPosition().y == 1) ) {
			if(!(square.getPosition().x == 0)) {
				squareEC = getSquare(square.getPosition().x-1, square.getPosition().y-2);
				if(squareEC.havePiece()) {
					if(squareEC.getPiece().getImagePath().contains("Brown")) {
						squareEC.setColor(this.colorRed);
						possibleSquares.add(squareEC);
					}
				}
				else {
					squareEC.setColor(this.colorMove);
					possibleSquares.add(squareEC);
				}
			}
			if(!(square.getPosition().x == 7) ) {
				squareEB = getSquare(square.getPosition().x+1, square.getPosition().y-2);
				if(squareEB.havePiece()) {
					if(squareEB.getPiece().getImagePath().contains("Brown")) {
						squareEB.setColor(this.colorRed);
						possibleSquares.add(squareEB);
					}
				}
				else {
					squareEB.setColor(this.colorMove);
					possibleSquares.add(squareEB);
				}
			}
		}

		Square squareCE;
		Square squareCD;

		if(!(square.getPosition().x == 0 || square.getPosition().x == 1) ) {
			if(!(square.getPosition().y == 7))
			{
				squareCD = getSquare(square.getPosition().x-2, square.getPosition().y+1);
				if(squareCD.havePiece()) {
					if(squareCD.getPiece().getImagePath().contains("Brown")) {
						squareCD.setColor(this.colorRed);
						possibleSquares.add(squareCD);
					}
				}
				else {
					squareCD.setColor(this.colorMove);
					possibleSquares.add(squareCD);
				}
			}
			if(!(square.getPosition().y == 0))
			{
				squareCE = getSquare(square.getPosition().x-2, square.getPosition().y-1);
				if(squareCE.havePiece()) {
					if(squareCE.getPiece().getImagePath().contains("Brown")) {
						squareCE.setColor(this.colorRed);
						possibleSquares.add(squareCE);
					}
				}
				else {
					squareCE.setColor(this.colorMove);
					possibleSquares.add(squareCE);
				}
			}
		}

		Square squareBD;
		Square squareBE;

		if( !(square.getPosition().x == 7 || square.getPosition().x == 6) ) {	
			if(!(square.getPosition().y == 7))
			{
				squareBD = getSquare(square.getPosition().x+2, square.getPosition().y+1);			
				
				if(squareBD.havePiece()) {
					if(squareBD.getPiece().getImagePath().contains("Brown")) {
						squareBD.setColor(this.colorRed);
						possibleSquares.add(squareBD);
					}
				}
				else {
					squareBD.setColor(this.colorMove);
					possibleSquares.add(squareBD);
				}
			}
			if(!(square.getPosition().y == 0) ) {
				squareBE = getSquare(square.getPosition().x+2, square.getPosition().y-1);
				if(squareBE.havePiece()) {
					if(squareBE.getPiece().getImagePath().contains("Brown")) {
						squareBE.setColor(this.colorRed);
						possibleSquares.add(squareBE);
					}
				}
				else {
					squareBE.setColor(this.colorMove);
					possibleSquares.add(squareBE);
				}
			}
		}
		t.testeCavaloWhite();
		t.setPosicaoX(square.getPosition().x);
		t.setPosicaoY(square.getPosition().y);
	}
	private void possiveisMovimentosDoCavaloBrown(Square square) {
		t.setRetornoEsperadoDeY(square.getPosition().y);
		t.setRetornoEsperadoDeX(square.getPosition().x);
		Square squareDC, squareDB;

		if( !(square.getPosition().y == 6 || square.getPosition().y ==7) ) {
			
			if(!(square.getPosition().x == 0)) {
				squareDC = getSquare(square.getPosition().x-1, square.getPosition().y+2);
				if(squareDC.havePiece()) {
					if( squareDC.getPiece().getImagePath().contains("White")) {
						squareDC.setColor(this.colorRed);
						possibleSquares.add(squareDC);
					}
				}
				else {
					squareDC.setColor(this.colorMove);
					possibleSquares.add(squareDC);
				}
				
			}
				
			if(!(square.getPosition().x == 7) ) {
				squareDB = getSquare(square.getPosition().x+1, square.getPosition().y+2);
				if(squareDB.havePiece()) {
					if(squareDB.getPiece().getImagePath().contains("White")) {
						squareDB.setColor(this.colorRed);
						possibleSquares.add(squareDB);
					}
				}
				else {
					squareDB.setColor(this.colorMove);
					possibleSquares.add(squareDB);
				}
				
			}			
		}
		
		
		Square squareEC;
		Square squareEB;	
		if( !(square.getPosition().y == 0 || square.getPosition().y == 1) ) {
			if(!(square.getPosition().x == 0)) {
				squareEC = getSquare(square.getPosition().x-1, square.getPosition().y-2);
				if(squareEC.havePiece()) {
					if(squareEC.getPiece().getImagePath().contains("White")) {
						squareEC.setColor(this.colorRed);
						possibleSquares.add(squareEC);
					}
				}
				else {
					squareEC.setColor(this.colorMove);
					possibleSquares.add(squareEC);
				}
			}
			if(!(square.getPosition().x == 7) ) {
				squareEB = getSquare(square.getPosition().x+1, square.getPosition().y-2);
				if(squareEB.havePiece()) {
					if(squareEB.getPiece().getImagePath().contains("White")) {
						squareEB.setColor(this.colorRed);
						possibleSquares.add(squareEB);
					}
				}
				else {
					squareEB.setColor(this.colorMove);
					possibleSquares.add(squareEB);
				}
			}
		}
		Square squareCE;
		Square squareCD;

		if(!(square.getPosition().x == 0 || square.getPosition().x == 1) ) {
			if(!(square.getPosition().y == 7))
			{
				squareCD = getSquare(square.getPosition().x-2, square.getPosition().y+1);
				if(squareCD.havePiece()) {
					if(squareCD.getPiece().getImagePath().contains("White")) {
						squareCD.setColor(this.colorRed);
						possibleSquares.add(squareCD);
					}
				}
				else {
					squareCD.setColor(this.colorMove);
					possibleSquares.add(squareCD);
				}
			}
			if(!(square.getPosition().y == 0))
			{
				squareCE = getSquare(square.getPosition().x-2, square.getPosition().y-1);
				if(squareCE.havePiece()) {
					if(squareCE.getPiece().getImagePath().contains("White")) {
						squareCE.setColor(this.colorRed);
						possibleSquares.add(squareCE);
					}
				}
				else {
					squareCE.setColor(this.colorMove);
					possibleSquares.add(squareCE);
				}
			}
		}
		
		Square squareBD;
		Square squareBE;
		
		if( !(square.getPosition().x == 7 || square.getPosition().x == 6) ) {	
			if(!(square.getPosition().y == 7))
			{
				squareBD = getSquare(square.getPosition().x+2, square.getPosition().y+1);			
				if(squareBD.havePiece()) {
					if(squareBD.getPiece().getImagePath().contains("White")) {
						squareBD.setColor(this.colorRed);
						possibleSquares.add(squareBD);
					}
				}
				else {
					squareBD.setColor(this.colorMove);
					possibleSquares.add(squareBD);
				}
			}
			if(!(square.getPosition().y == 0) ) {
				squareBE = getSquare(square.getPosition().x+2, square.getPosition().y-1);
				if(squareBE.havePiece()) {
					if(squareBE.getPiece().getImagePath().contains("White")) {
						squareBE.setColor(this.colorRed);
						possibleSquares.add(squareBE);
					}
				}
				else {
					squareBE.setColor(this.colorMove);
					possibleSquares.add(squareBE);
				}
			}
		}
		t.testeCavaloBrown();
		t.setPosicaoX(square.getPosition().x);
		t.setPosicaoY(square.getPosition().y);
	}
	
	private void possiveisMovimentosDoReiWhite(Square square) {
		t.setRetornoEsperadoDeY(square.getPosition().y);
		t.setRetornoEsperadoDeX(square.getPosition().x);
		for(int i = square.getPosition().x, j = square.getPosition().y; i<square.getPosition().x+1  || j< square.getPosition().y+1; i++, j++) {					
			
			if(i == 7 || j == 7) {
				break;
			}
		
			Square squareBDR = getSquare(i+1, j+1);
			
			if(squareBDR.getPosition().y == 7 && !squareBDR.havePiece()) {
				squareBDR.setColor(this.colorMove);
				possibleSquares.add(squareBDR);
				
				break;
			}
			if(squareBDR.havePiece()) 
			{
				if(squareBDR.getPiece().getImagePath().contains("Brown")) {
					squareBDR.setColor(this.colorRed);
					possibleSquares.add(squareBDR);
					break;
				}
				else {
					break;

				}
			}
			squareBDR.setColor(this.colorMove);
			possibleSquares.add(squareBDR);
		}
		
		for(int i = square.getPosition().x, j = square.getPosition().y; i<square.getPosition().x+1  || j> square.getPosition().y-1; i++, j--) {
			
			if(i == 7 || j == 0) {
				break;
			}
			
			Square squareBER = getSquare(i+1, j-1);
			
			if(squareBER.getPosition().y == 0 && !squareBER.havePiece()) {
				squareBER.setColor(this.colorMove);
				possibleSquares.add(squareBER);
				break;
			}
			
			if(squareBER.havePiece()) 
			{
				if(squareBER.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareBER.setColor(this.colorRed);
					possibleSquares.add(squareBER);
					break;
				}
			}
			
			squareBER.setColor(this.colorMove);
			possibleSquares.add(squareBER);
		}
		
		for(int i = square.getPosition().x, j = square.getPosition().y; i>square.getPosition().x-1  || j< square.getPosition().y+1; i--, j++) {
			
			if(i == 0 || j == 7) {
				
				break ;
			}
			
			Square squareCDR = getSquare(i-1, j+1);
			
			if(squareCDR.getPosition().y == 7 && !squareCDR.havePiece()) {
				squareCDR.setColor(this.colorMove);
				possibleSquares.add(squareCDR);
				break;
			}
			
			if(squareCDR.havePiece()) 
			{
				if(squareCDR.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareCDR.setColor(this.colorRed);
					possibleSquares.add(squareCDR);
					break;
				}
			}
			
			squareCDR.setColor(this.colorMove);
			possibleSquares.add(squareCDR);
		}
		for(int i = square.getPosition().x, j = square.getPosition().y; i>square.getPosition().x-1  || j> square.getPosition().y-1; i--, j--) {
			
			if(i == 0 || j == 0) {
				break;
			}
			
			
			Square squareCDR = getSquare(i-1, j-1);
			
			if(squareCDR.getPosition().y == 0 && !squareCDR.havePiece()) {
				
				squareCDR.setColor(this.colorMove);
				possibleSquares.add(squareCDR);
				break;
			}
			
			if(squareCDR.havePiece()) 
			{
				if(squareCDR.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareCDR.setColor(this.colorRed);
					possibleSquares.add(squareCDR);
					break;
				}
			}
			squareCDR.setColor(this.colorMove);
			possibleSquares.add(squareCDR);
		}
		//implementação da torre
		for(int i = square.getPosition().y+1; i <= square.getPosition().y+1 && square.getPosition().y+1!=8; i++) {
			Square squareDR = getSquare(square.getPosition().x, i);
			if(squareDR.havePiece()) 
			{
				if(squareDR.getPiece().getImagePath().contains("Brown")) {
					squareDR.setColor(this.colorRed);
					possibleSquares.add(squareDR);
					break;
				}
				else {
					break;

				}
			}
			squareDR.setColor(this.colorMove);
			possibleSquares.add(squareDR);
		}
		
		for(int i = square.getPosition().y-1; i>=square.getPosition().y-1 && square.getPosition().y !=0 ; i--) {
			Square squareER = getSquare(square.getPosition().x, i);
			if(squareER.havePiece()) 
			{
				if(squareER.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareER.setColor(this.colorRed);
					possibleSquares.add(squareER);
					break;
				}
			}
			squareER.setColor(this.colorMove);
			possibleSquares.add(squareER);
		}
		
		for(int i = square.getPosition().x+1; i<=square.getPosition().x+1 && square.getPosition().x+1!=8; i++) {
			Square squareCR = getSquare(i, square.getPosition().y);
			if(squareCR.havePiece()) 
			{
				if(squareCR.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareCR.setColor(this.colorRed);
					possibleSquares.add(squareCR);
					break;
				}
			}
			squareCR.setColor(this.colorMove);
			possibleSquares.add(squareCR);
		}
		
		for(int i = square.getPosition().x-1; i>=square.getPosition().x-1 && square.getPosition().x !=0 ; i--) {
			Square squareBR = getSquare(i, square.getPosition().y);
			if(squareBR.havePiece()) 
			{
				if(squareBR.getPiece().getImagePath().contains("White")) 
					break;
				else {
					squareBR.setColor(this.colorRed);
					possibleSquares.add(squareBR);
					break;
				}
			}
			squareBR.setColor(this.colorMove);
			possibleSquares.add(squareBR);
		}		
		t.testeReiWhite();
		t.setPosicaoX(square.getPosition().x);
		t.setPosicaoY(square.getPosition().y);
	}
	
	private void possiveisMovimentosDoReiBrown(Square square) {
		t.setRetornoEsperadoDeY(square.getPosition().y);
		t.setRetornoEsperadoDeX(square.getPosition().x);
		for(int i = square.getPosition().x, j = square.getPosition().y; i<square.getPosition().x+1  || j< square.getPosition().y+1; i++, j++) {
				/* BISPO NAS BORDAS DIREITA E INFERIOR*/
				if(i == 7 || j == 7) {
					break;
				}
			
				Square squareBDRB = getSquare(i+1, j+1);
				
				if(squareBDRB.getPosition().y == 7 && !squareBDRB.havePiece()) {
					squareBDRB.setColor(this.colorMove);
					possibleSquares.add(squareBDRB);
					
					break;
				}
				if(squareBDRB.havePiece()) 
				{
					if(squareBDRB.getPiece().getImagePath().contains("White")) {
						squareBDRB.setColor(this.colorRed);
						possibleSquares.add(squareBDRB);
						break;
					}
					else {
						break;
	
					}
				}
				squareBDRB.setColor(this.colorMove);
				possibleSquares.add(squareBDRB);
			}
			
			for(int i = square.getPosition().x, j = square.getPosition().y; i<square.getPosition().x+1  || j> square.getPosition().y-1; i++, j--) {
				if(i == 7 || j == 0) {
					break;
				}
				
				Square squareBERB = getSquare(i+1, j-1);
				
				if(squareBERB.getPosition().y == 0 && !squareBERB.havePiece()) {
					squareBERB.setColor(this.colorMove);
					possibleSquares.add(squareBERB);
					break;
				}
				
				if(squareBERB.havePiece()) 
				{
					if(squareBERB.getPiece().getImagePath().contains("Brown")) 
						break;
					else {
						squareBERB.setColor(this.colorRed);
						possibleSquares.add(squareBERB);
						break;
					}
				}
				
				squareBERB.setColor(this.colorMove);
				possibleSquares.add(squareBERB);
			}
			
			for(int i = square.getPosition().x, j = square.getPosition().y; i>square.getPosition().x-1  || j< square.getPosition().y+1; i--, j++) {
				
				/* BISPO NAS BORDAS DIREITA E SUPERIOR*/
				
				if(i == 0 || j == 7) {
					
					break ;
				}
				
				Square squareCDRB = getSquare(i-1, j+1);
				
				if(squareCDRB.getPosition().y == 7 && !squareCDRB.havePiece()) {
					squareCDRB.setColor(this.colorMove);
					possibleSquares.add(squareCDRB);
					break;
				}
				
				if(squareCDRB.havePiece()) 
				{
					if(squareCDRB.getPiece().getImagePath().contains("Brown")) 
						break;
					else {
						squareCDRB.setColor(this.colorRed);
						possibleSquares.add(squareCDRB);
						break;
					}
				}
				
				squareCDRB.setColor(this.colorMove);
				possibleSquares.add(squareCDRB);
			}
			for(int i = square.getPosition().x, j = square.getPosition().y; i>square.getPosition().x-1  || j> square.getPosition().y-1; i--, j--) {
	
				if(i == 0 || j == 0) {
					break;
				}
							
				Square squareCDRB = getSquare(i-1, j-1);
				
				if(squareCDRB.getPosition().y == 0 && !squareCDRB.havePiece()) {
					
					squareCDRB.setColor(this.colorMove);
					possibleSquares.add(squareCDRB);
					break;
				}
				
				if(squareCDRB.havePiece()) 
				{
					if(squareCDRB.getPiece().getImagePath().contains("Brown")) 
						break;
					else {
						squareCDRB.setColor(this.colorRed);
						possibleSquares.add(squareCDRB);
						break;
					}
				}
				squareCDRB.setColor(this.colorMove);
				possibleSquares.add(squareCDRB);
			}
			//implementação da torre
			
			for(int i = square.getPosition().y+1; i <= square.getPosition().y+1 && square.getPosition().y+1!=8; i++) {
				Square squareDRB = getSquare(square.getPosition().x, i);
				if(squareDRB.havePiece()) 
				{
					if(squareDRB.getPiece().getImagePath().contains("White")) {
						squareDRB.setColor(this.colorRed);
						possibleSquares.add(squareDRB);
						break;
					}
					else {
						break;
	
					}
				}
				squareDRB.setColor(this.colorMove);
				possibleSquares.add(squareDRB);
			}
			
			for(int i = square.getPosition().y-1; i>=square.getPosition().y-1 && square.getPosition().y !=0 ; i--) {
				Square squareERB = getSquare(square.getPosition().x, i);
				if(squareERB.havePiece()) 
				{
					if(squareERB.getPiece().getImagePath().contains("Brown")) 
						break;
					else {
						squareERB.setColor(this.colorRed);
						possibleSquares.add(squareERB);
						break;
					}
				}
				squareERB.setColor(this.colorMove);
				possibleSquares.add(squareERB);
			}
			
			for(int i = square.getPosition().x+1; i<=square.getPosition().x+1 && square.getPosition().x+1!=8; i++) {
				Square squareCRB = getSquare(i, square.getPosition().y);
				if(squareCRB.havePiece()) 
				{
					if(squareCRB.getPiece().getImagePath().contains("Brown")) 
						break;
					else {
						squareCRB.setColor(this.colorRed);
						possibleSquares.add(squareCRB);
						break;
					}
				}
				squareCRB.setColor(this.colorMove);
				possibleSquares.add(squareCRB);
			}
			
			for(int i = square.getPosition().x-1; i>=square.getPosition().x-1 && square.getPosition().x !=0 ; i--) {
				Square squareBRB = getSquare(i, square.getPosition().y);
				if(squareBRB.havePiece()) 
				{
					if(squareBRB.getPiece().getImagePath().contains("Brown")) 
						break;
					else {
						squareBRB.setColor(this.colorRed);
						possibleSquares.add(squareBRB);
						break;
					}
				}
				squareBRB.setColor(this.colorMove);
				possibleSquares.add(squareBRB);
			}
			t.testeReiBrown();
			t.setPosicaoX(square.getPosition().x);
			t.setPosicaoY(square.getPosition().y);
	}
	
	private void showPossibleMoves(Square square) {
		Piece piece = square.getPiece();
	
		if(piece.getTypeOfObject().equalsIgnoreCase("Peao")) {
			if(piece.getImagePath().contains("White"))
				possiveisMovimentosDoPeaoWhite(square);
			if(piece.getImagePath().contains("Brown"))
				possiveisMovimentosDoPeaoBrown(square);
		}
		if(piece.getTypeOfObject().equalsIgnoreCase("Torre")) {	
			if(piece.getImagePath().contains("White"))
				possiveisMovimentosDaTorreWhite(square);
			if(piece.getImagePath().contains("Brown"))
				possiveisMovimentosDaTorreBrown(square);				
		}
		if(piece.getTypeOfObject().equalsIgnoreCase("Cavalo")) {	
			if(piece.getImagePath().contains("White"))
				possiveisMovimentosDoCavaloWhite(square);
			if(piece.getImagePath().contains("Brown"))
				possiveisMovimentosDoCavaloBrown(square);
		}
		if(piece.getTypeOfObject().equalsIgnoreCase("Bispo")) {
			if(piece.getImagePath().contains("White"))
				possiveisMovimentosDoBispoWhite(square);
			if(piece.getImagePath().contains("Brown"))
				possiveisMovimentosDoBispoBrown(square);
		}
		if(piece.getTypeOfObject().equalsIgnoreCase("Rainha")) {
			if(piece.getImagePath().contains("White"))
				possiveisMovimentosDaRainhaWhite(square);
			if(piece.getImagePath().contains("Brown"))
				possiveisMovimentosDaRainhaBrown(square);
		}
		if(piece.getTypeOfObject().equalsIgnoreCase("Rei")) {
			if(piece.getImagePath().contains("White"))
				possiveisMovimentosDoReiWhite(square);
			if(piece.getImagePath().contains("Brown"))
				possiveisMovimentosDoReiBrown(square);
		}
	}
}
