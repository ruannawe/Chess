package Teste;
import junit.framework.TestCase;
import model.Square;

public class TestesUnitarios extends TestCase{
	Square s = new Square(); 
	
	private int retornoEsperadoDeY=0;	
	private int retornoEsperadoDeX=0;
	private int posicaoX=0;
	private int posicaoY=0;
	
	public TestesUnitarios(){
	
	}
	
	public int getPosicaoX() {
		return posicaoX;
	}

	public void setPosicaoX(int posicaoX) {
		this.posicaoX = posicaoX;
	}

	public int getPosicaoY() {
		return posicaoY;
	}

	public void setPosicaoY(int posicaoY) {
		this.posicaoY = posicaoY;
	}

	
	public TestesUnitarios(int retornoEsperadoDeY, int retornoEsperadoDeX){
		this.retornoEsperadoDeY=retornoEsperadoDeY;
		this.retornoEsperadoDeX=retornoEsperadoDeX;
	}
	
	public int getRetornoEsperadoDeY() {
		return retornoEsperadoDeY;
	}

	public int getRetornoEsperadoDeX() {
		return retornoEsperadoDeX;
	}

	public void setRetornoEsperadoDeX(int retornoEsperadoDeX) {
		this.retornoEsperadoDeX = retornoEsperadoDeX;
	}

	public void setRetornoEsperadoDeY(int retornoEsperadoDeY) {
		this.retornoEsperadoDeY = retornoEsperadoDeY;
	}
	
			
	
	public void testePeaoWhite() {
		if(posicaoX == retornoEsperadoDeX+2)      {
			assertEquals(posicaoX,retornoEsperadoDeX+2,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 ) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
		}
		
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
	}
	
	public void testePeaoBrown() {
		if(posicaoX == retornoEsperadoDeX+2)      {
			assertEquals(posicaoX,retornoEsperadoDeX+2,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 ) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
		}
		
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
	}
	
	public void testeTorreWhite() {
		if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
	}
	public void testeTorreBrown() {
		if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
	}
	
	public void testeBispoWhite() {
		if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
	}
	
	public void testeBispoBrown() {
		if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
	}
	
	public void testeCavaloWhite() {
		if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}

	}
	public void testeCavaloBrown() {
		if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
	}

	public void testeReiWhite() {
		if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}

	}
	public void testeReiBrown() {
		if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX+1 && posicaoY == retornoEsperadoDeY-1) {
			assertEquals(posicaoX,retornoEsperadoDeX+1,0);
			assertEquals(posicaoY,retornoEsperadoDeY-1,0);
		}
		else if(posicaoX == retornoEsperadoDeX-1 && posicaoY == retornoEsperadoDeY+1) {
			assertEquals(posicaoX,retornoEsperadoDeX-1,0);
			assertEquals(posicaoY,retornoEsperadoDeY+1,0);
		}

	}
}
